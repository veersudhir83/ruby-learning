student_names = ["John", "Reynolds", "Patel"]
puts "Students are #{student_names}"
puts "Sorted Students are #{student_names.sort!}"
puts "Reversed list of students #{student_names.reverse}"

array1 = Array.new(2) #creates an empty array with 2 elements, but more elements can be added
print "array1 is #{array1}"
puts "\n-----------------------------"
puts "array1 is empty? #{array1.empty?}"
array1[0] = 'John'
array1[1] = 'Victor'
array1[2] = 'Patel'
print array1
puts "\n-----------------------------"
puts array1
puts array1.length
puts "array1 is empty? #{array1.empty?}"