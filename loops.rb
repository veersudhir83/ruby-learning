student_names = ["John", "Reynolds", "Patel"]
# for loop
for i in 1..student_names.length
  puts "Student name at #{i-1} is #{student_names[i-1]}"
end

puts "-----------------"

# while loop
x = 10
y = 1
while x >= y
  if x % 2 == 0 then
    puts "#{x} is even"
  end
  x = x - 1
end

puts "-----------------"

# until loop
x = 1
y = 10
until x == y
  if x % 2 != 0 then
    puts "#{x} is odd"
  end
  x = x + 1
end