# Fixnum = numbers less than 2^62
# Bignum = numbers greater than 2^62

age = 28

# prints the class of age variable
puts age.class

puts age

# even? - returns true if age is even
puts age.even?

age = 22
# odd? - returns true if age is odd
puts age.odd?

# to_s - converts Integer value to String value
puts age.to_s.class
puts age.class

# to_f - converts Integer value to Float value
puts age.to_f
puts age.to_f.class
puts age.class