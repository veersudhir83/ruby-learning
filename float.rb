number = -5.69

puts number

# abs - prints absolute value
puts number.abs

# round - rounds the float to nearest integer
puts number.round
number2 = 4.49326
puts number2.round
puts number2.round(3)

# to_i - converts float value to integer value
puts number2.to_i
puts number2.to_i.class

# to_s - converts float value to string value
puts number2.class
puts number2.to_s.class

