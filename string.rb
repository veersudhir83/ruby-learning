first_name = 'Johnson'
last_name = 'Doe'
address = "1203, Walstreet, NY city, NY"
county = "Fairfax"

# class - prints the class of the variable
puts first_name.class

# concat - append the strings
puts "person's full name = ".concat((first_name.concat(" ").concat(last_name)))

# upcase and downcase - convert to upper and lower case
puts 'first_name in upper case = ' + first_name.upcase
puts 'first_name in lower case = ' + first_name.downcase

# using ! at the end of the function will update the original string
last_name.upcase!
puts 'last_name updated to upper case = ' + last_name

# capitalize - capitalizes the first letter of the string and converts remaining chars into smaller case
random_str = 'aBcDeFgH'
puts random_str + " after capitalized = " + random_str.capitalize!

# reverse - reverses the characters in the string
reverse_str = "desserts"
puts reverse_str + " after reverse = " + reverse_str.reverse

# delete - deletes the specified char/string from the original string
puts 'address without e = ' + address.delete!('e')

# replace - replace original string with new string
# gsub - replace specified substring of a string with new string globally (all over) in the original string
print county
county.replace('Glen dale dale')
print " replaced with -> ".concat(county).concat("\n")
county.gsub!('dale', 'ford')
print "'Glen dale dale'" + " replaced with -> ".concat(county).concat("\n")

# length - gives the length of string
# to_s - converts the object to string
puts "length of address string = ".concat(address.length.to_s)

# strip - removes the leading and trailing white spaces from the string
# lstrip and rstrip will strip respective sides
strip_str = '  some thing  '
puts "'" + strip_str + "' after strip = '" + strip_str.strip + "'"
puts "'" + strip_str + "' after lstrip = '" + strip_str.lstrip + "'"
puts "'" + strip_str + "' after rtrip = '" + strip_str.rstrip + "'"
