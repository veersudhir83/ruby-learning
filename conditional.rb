weather = 'SUNNY'
temperature = 38
isMarried = TRUE

if weather.capitalize == 'Raining' && (0..36).member?(temperature)
  print "I will stay home and watch movies on prime"
  if isMarried == TRUE
    print " with my wife :-)"
  end  
elsif weather.capitalize != 'Raining' && (36..45).member?(temperature)
  print "I will go out for a walk"
  if isMarried == TRUE
    print " with my wife :-)"
  end  
else
  print "I don't know what to do"  
end    
puts ""

number = 13
print number
if number.even?
  print " is Even number"
else
  print " is Odd Number"
end  
puts ""

grade = 'b+'
studentName = "John"
case(grade.upcase) 
when 'A+','A'
  puts "#{studentName} has great score"
when 'A-','B+','B'  
  puts "#{studentName} has average score"
when 'B-','C+','C'
  puts "#{studentName} has poor score"
else
  puts "#{studentName} has very bad score.. Can't be promoted"  
end  

creditScore = 699
case(creditScore) 
when 700..850
  puts 'Congratulation.. Credit Card Approved @ 3%'
when 650..700  
  puts 'Credit Card Approved @ 8%'
when 500..650
  puts 'Credit Card may get approved\n but at @ 14%'  
else
  puts 'Your application is rejected'
end  